import React, { useState } from "react";
import Button from "../../../components/Boutons/Button";

function FormEdit(props) {
  const [editValues, setEditValues] = useState({
    titreSaisi: props.livre.titre,
    auteurSaisi: props.livre.auteur,
    nbPages: props.livre.nbPages,
  });

  const handleChange = (e) => {
    setEditValues({
      ...editValues,
      [e.target.name]: e.target.value,
    });
  };

  const handleClick = () => {
    props.handleModificationLivre(props.livre.id, editValues);
  };

  return (
    <tr>
      <td>
        <input
          className="form-control"
          type="text"
          name="titreSaisi"
          value={editValues.titreSaisi}
          onChange={handleChange}
        />
      </td>
      <td>
        <input
          className="form-control"
          type="text"
          name="auteurSaisi"
          value={editValues.auteurSaisi}
          onChange={handleChange}
        />
      </td>
      <td>
        <input
          className="form-control"
          type="number"
          name="nbPages"
          value={editValues.nbPages}
          onChange={handleChange}
        />
      </td>
      <td>
        <Button className="btn-success" click={handleClick}>
          Valider
        </Button>
      </td>
      <td>
        <Button
          className="btn-secondary"
          click={() => props.handleEditLivre(0)}
        >
          Annuler
        </Button>
      </td>
    </tr>
  );
}

export default FormEdit;
