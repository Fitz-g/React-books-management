import React, { useState } from "react";
import Livre from "../../components/Livres/Livre";
import ForumlaireAjout from "./FormulaireAjout/ForumlaireAjout";
import FormEdit from "./FormulaireEdit/FormEdit";
import Alert from "../../components/Alerts/Alert";

function Livres(props) {
  const listLivres = [
    {
      id: 1,
      titre: "La vie est beau !",
      auteur: "Marc LAVOINE",
      nbPages: 200,
    },
    {
      id: 2,
      titre: "L'algorithme selon H2PROG",
      auteur: "Mathieu GASTON",
      nbPages: 220,
    },
    {
      id: 3,
      titre: "Le monde des animaux",
      auteur: "Albert PATRICK",
      nbPages: 500,
    },
    {
      id: 4,
      titre: "Le virus de l'Asie",
      auteur: "Tya MILO",
      nbPages: 120,
    },
    {
      id: 5,
      titre: "La France du 19ème",
      auteur: "Marc MERLIN",
      nbPages: 250,
    },
  ];

  const [livres, setLivres] = useState(listLivres);
  const [idLivreAmodifier, setIdLivreAmodifier] = useState(0);
  const [alertMessage, setAlertMessage] = useState(null);

  const handleDelete = (id) => {
    let newLivres = [...livres].filter((livre) => livre.id !== id);
    setLivres(newLivres);
    setAlertMessage({
      message: "Livre supprimé avec succès",
      type: "alert-danger",
    });
  };

  const handleAjoutLivre = (values) => {
    values.id = new Date().getTime();
    setLivres([
      ...livres,
      {
        id: values.id,
        titre: values.titreSaisi,
        auteur: values.auteurSaisi,
        nbPages: values.nbPages,
      },
    ]);
    props.fermerAjoutLivre();
    setAlertMessage({
      message: "Livre ajouté avec succès",
      type: "alert-success",
    });
  };

  const handleEditLivre = (id) => {
    setIdLivreAmodifier(id);
  };

  const handleModificationLivre = (id, newLivreValues) => {
    const indexLivreToEdit = livres.findIndex((livre) => livre.id === id);
    const newLivre = {
      id: id,
      titre: newLivreValues.titreSaisi,
      auteur: newLivreValues.auteurSaisi,
      nbPages: newLivreValues.nbPages,
    };

    const arrayListLivreCopy = [...livres];

    arrayListLivreCopy[indexLivreToEdit] = newLivre;

    setLivres(arrayListLivreCopy);
    setIdLivreAmodifier(0);
    setAlertMessage({
      message: "Livre modifié avec succès",
      type: "alert-warning",
    });
  };

  return (
    <>
      {alertMessage && (
        <Alert className={alertMessage.type}>{alertMessage.message}</Alert>
      )}
      <table className="table text-center">
        <thead>
          <tr className="table-dark">
            <th scope="col">Titre</th>
            <th scope="col">Auteur</th>
            <th scope="col">Nombre de pages</th>
            <th colSpan={2}>Action</th>
          </tr>
        </thead>
        <tbody>
          {livres.map((livre) => {
            if (livre.id !== idLivreAmodifier) {
              return (
                <Livre
                  key={livre.id}
                  livre={livre}
                  handleDelete={handleDelete}
                  handleEditLivre={handleEditLivre}
                />
              );
            } else {
              return (
                <FormEdit
                  key={livre.id}
                  livre={livre}
                  handleEditLivre={handleEditLivre}
                  handleModificationLivre={handleModificationLivre}
                />
              );
            }
          })}
        </tbody>
      </table>
      {props.ajoutFormulaire ? (
        <ForumlaireAjout handleAjoutLivre={handleAjoutLivre} />
      ) : null}
    </>
  );
}

export default Livres;
