import React, { useState } from "react";

import classes from "./formulaireAjout.module.css";
import Button from "../../../components/Boutons/Button";

function ForumlaireAjout(props) {
  const [formValues, setFormValues] = useState({
    titreSaisi: "",
    auteurSaisi: "",
    nbPages: "",
  });

  const handleValidationForm = (e) => {
    e.preventDefault();
    props.handleAjoutLivre(formValues);
    setFormValues({
      titreSaisi: "",
      auteurSaisi: "",
      nbPages: "",
    });
  };

  const handleChange = (e) => {
    setFormValues({
      ...formValues,
      [e.target.name]: e.target.value,
    });
  };

  return (
    <>
      <h2
        className={`text-center text-primary fw-bold my-3 ${classes.formulaire}`}
      >
        AFFICHAGE DU FORMULAIRE D'AJOUT
      </h2>
      <form className="mb-5">
        <div className="form-group my-3">
          <label htmlFor="titreSaisi">Titre du livre</label>
          <input
            type="text"
            className="form-control"
            name="titreSaisi"
            id="titreSaisi"
            value={formValues.titreSaisi}
            onChange={handleChange}
          />
        </div>
        <div className="form-group my-3">
          <label htmlFor="auteurSaisi">Auteur</label>
          <input
            type="text"
            className="form-control"
            name="auteurSaisi"
            id="auteurSaisi"
            value={formValues.auteurSaisi}
            onChange={handleChange}
          />
        </div>
        <div className="form-group my-3">
          <label htmlFor="nbPages">Nombre de pages</label>
          <input
            type="number"
            className="form-control"
            name="nbPages"
            id="nbPages"
            value={formValues.nbPages}
            onChange={handleChange}
          />
        </div>
        <Button className="btn-primary" click={(e) => handleValidationForm(e)}>
          Ajouter
        </Button>
      </form>
    </>
  );
}

export default ForumlaireAjout;
