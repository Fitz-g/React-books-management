import { useState } from "react";
import "./App.css";

import Button from "./components/Boutons/Button";
import TitreH1 from "./components/Titres/TitreH1";
import Livres from "./containers/Livres/Livres";

function App() {
  const [clickAdd, setClickAdd] = useState(false);

  const handleClick = (e) => {
    setClickAdd((prevState, props) => !prevState);
  };

  return (
    <div className="container">
      <TitreH1>Page listant les livres</TitreH1>
      <Livres
        ajoutFormulaire={clickAdd}
        fermerAjoutLivre={() => setClickAdd(false)}
      />
      <Button className="btn-success w-100 mb-5" click={(e) => handleClick(e)}>
        {!clickAdd ? "Ajouter" : "Fermer l'ajout"}
      </Button>
    </div>
  );
}

export default App;
