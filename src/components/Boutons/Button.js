import React from "react";

function Button(props) {
  return (
    <button
      type={`${props.type ?? "submit"}`}
      className={`btn ${props.className}`}
      onClick={props.click}
    >
      {props.children}
    </button>
  );
}

export default Button;
