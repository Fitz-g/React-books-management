import classes from "./title.module.css";

function TitreH1(props) {
  return (
    <h1
      className={`${classes.policeTitre} text-center text-white bg-primary rounded mt-2 p-2 border border-dark`}
    >
      {props.children}
    </h1>
  );
}

export default TitreH1;
