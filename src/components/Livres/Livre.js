import React from "react";

import Button from "../Boutons/Button";

function Livre(props) {
  const handleEdit = () => {
    props.handleEditLivre(props.livre.id);
  };
  return (
    <tr>
      <td>{props.livre.titre}</td>
      <td>{props.livre.auteur}</td>
      <td>{props.livre.nbPages}</td>
      <td>
        <Button className="btn-warning" click={handleEdit}>
          Modifier
        </Button>
      </td>
      <td>
        <Button
          className="btn-danger"
          click={() => props.handleDelete(props.livre.id)}
        >
          Suprimer
        </Button>
      </td>
    </tr>
  );
}

export default Livre;
